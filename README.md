# Slackers 

Slackware BINARY Repository by Conraid

## Repository for Slackware Current packages

Repository contains SlackBuild script to make package from binary files.

Download source code:

  source .info
  curl -JOL $DOWNLOAD

or

  wget [--no-check-certificate] [--content-disposition] $DOWNLOAD

wget, with some servers, doesn't set the correct filename

If exists FILENAME variable means that it contains the filename.

if exists DOWNLOAD__* extra variables, do also

 wget --no-check-certificate --content-disposition $DOWNLOAD [$DOWNLOAD_1] [$DOWNLOAD_2]

## Build

To build:

  sh .SlackBuild

This command builds a package which will be created in the same directory.

N.B.
These SlackBuilds do not build from source, they simply repackages the binaries.

## Note

If you have any questions or requests, you can contact me at slackers.it (at) gmail (dot) com

All scrips are released under the GPL License.


